# Semestrální práce - grafické prostředí pro SQL lite
Patrik Böhm
Tato semestrální práce se věnuje vytvoření jednoduchého grafického prostředí pro připojení/vytvoření databáze SQL lite.
Pracoval jsem v programu Qt Creator, kde jsem vytvořil jednotlivé objekty
(tlačítka a labely) a následně je propojil se sloty. 
Soubory widget.cpp a widget.h popisují úvodní okno, kde jsou napsané pokyny pro používání. Soubory okno.h a okno.cpp popisují připojení/vytvoření databáze a metody pro přidání, vymazání, obnovení a hledání jednotlivých položek.
Pro zobrazení jsem použil QTableView, který jsem popsal QSqlTable modelem.
Při kliknutí na hlavičku jednotlivých sloupců se řádky řadí. Při kliknutí na přidat se vytvoří nový řádek. Při napsání poslední položky do řádku je nutné potvrdit enterem, jinak pak nelze vložit nový řádek. Při mazání řádku stačí kliknout na jednu položku v řádku a potvrdit to tlačítkem "Vymazat". 
Tato práce mi ukázala jiný pohled na programování v Qt Creator.
