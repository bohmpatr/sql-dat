// okno, kde se nacita databaze
#include "okno.h"
#include "widget.h"
#include "ui_okno.h"
#include<QDialog>
#include<QPushButton>
#include<QSqlDatabase>
#include<QDebug>
#include<QSqlQuery>
#include<QSqlTableModel>


Okno::Okno(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Okno)
{
    ui->setupUi(this);
    setWindowTitle("Databáze");
    db = QSqlDatabase::addDatabase("QSQLITE");
    // databáze bude vytvorena, tam kde je program spusten
    db.setDatabaseName("./databaze.db");
    if(db.open())
{
        //qDebug("open");
    }
    else
{
        QMessageBox::information(this, "Pozor","Něco je špatně");
        exit(EXIT_FAILURE);
    }
    query = new QSqlQuery(db);
    query->exec("CREATE TABLE IF NOT EXISTS Filmy (Jméno TEXT,Žánr TEXT,Hodnocení	INTEGER,Rok INTEGER, Poznámky	TEXT, PRIMARY KEY(Jméno));");

    model = new QSqlTableModel(this, db);

    model->setTable("Filmy");

    model->select();
    model->setHeaderData(0, Qt::Horizontal, "Jméno");
    model->setHeaderData(1, Qt::Horizontal, "Žánr");
    model->setHeaderData(2, Qt::Horizontal, "Hodnocení");
    model->setHeaderData(3, Qt::Horizontal, "Rok");
    model->setHeaderData(4, Qt::Horizontal, "Poznámky");
    model->setSort(2, Qt::DescendingOrder);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionsClickable(true);
    ui->tableView->horizontalHeader()->setSectionsMovable(true);
    ui->tableView->setSortingEnabled(true);


    ui->tableView->setModel(model);





}

Okno::~Okno()
{

    delete ui;
}





void Okno::on_pushButton_clicked()
{
    model->insertRow(model->rowCount());

}


void Okno::on_pushButton_2_clicked()
{
    model->removeRow(row);
}


void Okno::on_tableView_clicked(const QModelIndex &index)
{       // získám index řádku -> pro nassledne odstraneni
     row = index.row();
}


// tlacitko na obnoveni
void Okno::on_pushButton_3_clicked()
{
    // asi nemusim vytvaret znova model, ale nejak mi to jinak nefungovalo
    model = new QSqlTableModel(this, db);

    model->setTable("Filmy");

    model->select();
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionsClickable(true);
    ui->tableView->horizontalHeader()->setSectionsMovable(true);
    ui->tableView->setSortingEnabled(true);


    ui->tableView->setModel(model);

}

// tlacitko na hledani podle jmena
void Okno::on_pushButton_4_clicked()
{
    // vratim to co jsem napsal
       model = new QSqlTableModel(this, db);
       const QString edit = ui->lineEdit->text();
        model->setTable("Filmy");
       //qDebug()<<edit;
       //model->setFilter("Název == Pád do tmy");
      model->setFilter(QString("Jméno = '%1'").arg(edit));
      ui->lineEdit->clear();
       model->select();
       ui->tableView->setModel(model);


}


void Okno::on_pushButton_5_clicked()
{
    model = new QSqlTableModel(this, db);
    const QString edit = ui->lineEdit->text();
     model->setTable("Filmy");
    //qDebug()<<edit;
    //model->setFilter("Název == Pád do tmy");
   model->setFilter(QString("Žánr = '%1'").arg(edit));
   ui->lineEdit->clear();
    model->select();
    ui->tableView->setModel(model);
}

