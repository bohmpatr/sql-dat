// uvodni okno
#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    setWindowTitle("Úvodní okno");



}

Widget::~Widget()
{
    delete ui;
}

// tlacitko ktere otevre okno, po korektnim pripojeni databaze
// potrebuju otevrik novy okno (Okno), a schovat okno(Widget)
void Widget::on_ok_clicked()
{
// schovam okno
    this->hide();
// vytvorim objekt okno, ktere potom execem spustim
    Okno okno;
    okno.setModal(true);

    okno.exec();




}

