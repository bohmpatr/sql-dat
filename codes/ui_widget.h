/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 6.1.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QPushButton *ok;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_2;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(779, 581);
        ok = new QPushButton(Widget);
        ok->setObjectName(QString::fromUtf8("ok"));
        ok->setGeometry(QRect(610, 500, 121, 41));
        layoutWidget = new QWidget(Widget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 20, 551, 531));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamilies({QString::fromUtf8("Comic Sans MS")});
        font.setPointSize(10);
        font.setBold(true);
        label->setFont(font);
        label->setTextFormat(Qt::RichText);

        verticalLayout->addWidget(label);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);
        label_3->setTextFormat(Qt::RichText);

        verticalLayout->addWidget(label_3);

        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font);
        label_4->setTextFormat(Qt::RichText);

        verticalLayout->addWidget(label_4);

        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font);
        label_5->setTextFormat(Qt::RichText);

        verticalLayout->addWidget(label_5);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);
        label_2->setTextFormat(Qt::RichText);

        verticalLayout->addWidget(label_2);


        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QCoreApplication::translate("Widget", "Widget", nullptr));
        ok->setText(QCoreApplication::translate("Widget", "Rozum\303\255m", nullptr));
        label->setText(QCoreApplication::translate("Widget", "V\303\255t\303\241m V\303\241s v aplikaci na jednoduchou spr\303\241vu film\305\257", nullptr));
        label_3->setText(QCoreApplication::translate("Widget", "Jm\303\251na pi\305\241te s velk\303\275m po\304\215\303\241te\304\215n\303\255m p\303\255smenem", nullptr));
        label_4->setText(QCoreApplication::translate("Widget", "Ur\304\215ete si vlastn\303\255 \305\241k\303\241lu hodnocen\303\255 ", nullptr));
        label_5->setText(QCoreApplication::translate("Widget", "P\305\231idan\303\241 data potvr\304\217te ENTEREM", nullptr));
        label_2->setText(QCoreApplication::translate("Widget", "P\305\231eji hodn\304\233 z\303\241bavy a filmov\303\275ch z\303\241\305\276itk\305\257", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
