#ifndef OKNO_H
#define OKNO_H

#include <QDialog>
#include<QSqlQueryModel>
#include<QSqlTableModel>
#include"widget.h"



namespace Ui {
class Okno;
}

class Okno : public QDialog
{
    Q_OBJECT



public:
    explicit Okno(QWidget *parent = nullptr);
    ~Okno();





private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_tableView_clicked(const QModelIndex &index);

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::Okno *ui;
    QSqlDatabase db;
    QSqlQuery *query;
    QSqlTableModel *model;

    int row;

};

#endif // OKNO_H
