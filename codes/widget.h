#ifndef WIDGET_H
#define WIDGET_H
#include"okno.h"
#include <QWidget>
#include<QCoreApplication>
#include<QSqlDatabase>
#include<QSqlQuery>
#include<QSqlError>
#include<QDebug>
#include<QString>
#include<QApplication>
#include<QMessageBox>
#include<QtWidgets>


QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT


public:
    Widget(QWidget *parent = nullptr);
    ~Widget();


private slots:
    void on_ok_clicked();

private:
    Ui::Widget *ui;
};
#endif // WIDGET_H
